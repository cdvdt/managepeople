﻿using System;
namespace ManagePeople
{
	public class Info
	{
        public string phone { get; set; }
        public Address address;
    }

    public class Interest
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class PersonalInfo : Info
    {
        public Interest[] interests { get; set; }
    }

	public class OrganizationNode {
		public int id { get; set; }
		public string name { get; set; }
	}

	public class Member : OrganizationNode {
        public PersonalInfo info { get; set; }
    }

	public class Subdivision : OrganizationNode {
		public Member[] members { get; set; }
        public Subdivision[] subdivisions { get; set; }
        public Info info { get; set; }
	}

	public class TaskData {
		public int id { get; set; }
		public Task task;
		public Member member;
	}

	public class Task : OrganizationNode {
		public TaskData[] taskData { get; set; }
		public DateTime date { get; set; }
	}
}
