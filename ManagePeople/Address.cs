﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePeople
{
    public class Street
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    public class Neighborhood
    {
        public int id { get; set; }
        public string name { get; set; }
        public Street[] streets;
    }

    public class City
    {
        public int id { get; set; }
        public string name { get; set; }
        public Neighborhood[] neighborhoods;
    }

    public class State
    {
        public int id { get; set; }
        public string name { get; set; }
        public string abbreviation { get; set; }
        public City[] cities;
    }

    public class Country
    {
        public int id { get; set; }
        public string name { get; set; }
        public string abbreviation { get; set; }
        public State[] states;
    }

    public class Address
    {
        public int id { get; set; }
        public Street number { get; set; }
        public Neighborhood neighborhood { get; set; }
        public City city;
        public State state;
        public Country country;
    }
}
