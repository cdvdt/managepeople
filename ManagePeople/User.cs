﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteDB;
using System.Security.Cryptography;

namespace ManagePeople
{
	public class User : Member
    {
        public string username { get; set; }
        public string login { get; set; }
        public Byte[] password { get; set; }
		public Byte[] salt { get; set; }
    }

	public class AuthenticationManager {
		public class DatabaseNotFoundException : System.Exception {
			
		}

		public static User Authenticate(string login, string password, IDatabaseConnection databaseConnection) {
			LiteDatabase database = databaseConnection.GetDatabase();

			var users = database.GetCollection<User>("users");
			var user = users.FindOne(x => x.login == login);
			if (user == null)
				return null;
			
			var sha256 = SHA256.Create();
			if (user.password.SequenceEqual(sha256.ComputeHash(System.Text.Encoding.Unicode.GetBytes(password).Concat<Byte>(user.salt).ToArray()))) {
				return user;
			}

			return null;
		}

		public static User AddUser(string login, string password, IDatabaseConnection databaseConnection) {
			LiteDatabase database = databaseConnection.GetDatabase();

            var users = database.GetCollection<User>("users");
			if (users.FindOne(x => x.login == login) == null)
			{
				var user = new User();
				user.login = login;

				var rngCsp = new RNGCryptoServiceProvider();
				var random = new Random(DateTime.Now.Millisecond);

				user.salt = new byte[random.Next(0, 32)];

				rngCsp.GetBytes(user.salt);
				var sha256 = SHA256.Create();

				user.password = sha256.ComputeHash(System.Text.Encoding.Unicode.GetBytes(password).Concat<Byte>(user.salt).ToArray());

				users.Insert(user);
				users.EnsureIndex(x => x.login);

				return user;
			}

            return null;
		}
	}
}
