﻿using System;
using LiteDB;

namespace ManagePeople
{
    public interface IDatabaseConnection
    {
		LiteDatabase GetDatabase();
    }
}
